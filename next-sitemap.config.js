/** @type {import('next-sitemap').IConfig} */
module.exports = {
    // YOUR CONFIG
    siteUrl: process.env.SITE_URL || "http://localhost:3000/",
    generateRobotsTxt: true, // (optional)
  }